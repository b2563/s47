// document-querySelector -> use it to retrieve an element from the webpage
/*
	document.getElementById('txt-first-name');
	document.getElementsByClassName('txt-input');
	document.getElementsByTagName('input')
*/
const textFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

// action is considered as an event
textFirstName.addEventListener('keyup', (event) => {

	spanFullName.innerHTML = textFirstName.value;

});

textFirstName.addEventListener('keyup', (event) => {

	console.log(event.target);
	console.log(event.target.value);

});
